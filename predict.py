import sys
import warnings
import numpy as np
from skimage.color import rgb2yiq, rgb2hed, rgb2hsv, rgb2lab, rgb2luv
from skimage.io import imread
try:
    from sklearn.externals import joblib
except ImportError:
    import joblib
from ocpp import OCPP


def extract_feature(image):
    feature = [OCPP(image).reshape(1, -1)]
    fe = [rgb2yiq, rgb2hed, rgb2hsv, rgb2lab, rgb2luv]
    for f in fe:
        image = f(image)
        image = (image - image.min()) / (image.max() - image.min())
        image = (255 * image).astype(np.uint8)
        feat = OCPP(image).reshape(1, -1)
        feature.append(feat)
    feat = np.hstack(feature)
    return feat


def predict(img, model):
    feature = extract_feature(img)
    score = model.predict(feature)
    return score[0]


def load_model(name='MODEL.pkl'):
    return joblib.load(name)


def warn(*args, **kwargs):
    pass


if __name__ == "__main__":
    warnings.warn = warn
    image = imread(sys.argv[1])
    model = load_model()
    quality_score = predict(image, model)
    print("\033cOCPP score: {score}".format(score=quality_score))
