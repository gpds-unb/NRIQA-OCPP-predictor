#include <gc/gc_cpp.h>
#include "AbstractLBPFeatureExtractor.hpp"

#define CV_BGR2GRAY COLOR_BGR2GRAY

void AbstractLBPFeatureExtractor::init(Mat *mat, int i) {
    setLength(i);
    setHeight(mat[0].rows);
    setWidth(mat[0].cols);
    setFrames(mat, i);
}

void AbstractLBPFeatureExtractor::setFrames(Mat *frames, int numberOfFrames) {
    auto isGray = [](Mat& m) {
      return m.channels() == 1;
    };
    greyImg = new(UseGC) Mat[Length];
    for (int i = 0; i < numberOfFrames; i++) {
        Mat temp = frames[i];
        if (!isGray(temp)) {
          cvtColor(temp, temp, CV_BGR2GRAY);
          temp.convertTo(greyImg[i], CV_8UC1);
        } else {
          greyImg[i] = temp;
        }
    }
}

void AbstractLBPFeatureExtractor::setLength(int l) {
    this->Length = l;
}

void AbstractLBPFeatureExtractor::setHeight(int height) {
    this->height = height;
}

void AbstractLBPFeatureExtractor::setWidth(int width) {
    this->width = width;
}

AbstractLBPFeatureExtractor::AbstractLBPFeatureExtractor(Mat *mat, int i) {
    init(mat, i);
}

AbstractLBPFeatureExtractor::AbstractLBPFeatureExtractor(vector<Mat> v) {
    init(&v[0], v.size());
}

AbstractLBPFeatureExtractor::AbstractLBPFeatureExtractor(Mat m){
    int channels = m.channels();
    vector<Mat> frames(channels);
    split(m, frames);
    init(&frames[0], frames.size());
}
