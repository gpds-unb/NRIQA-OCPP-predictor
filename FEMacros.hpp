//
// Created by pedro on 25/09/15.
//

#ifndef VQANOREF_FEMACROS_HPP
#define VQANOREF_FEMACROS_HPP

#include <boost/math/constants/constants.hpp>

#define BOUND(x, l, u) { (x) = (x) > (l) ? (x) : (l); (x) = (x) < (u) ? (x) : (u); };
#define POW(nBit) (1 << (nBit))
#define FREE(ptr) {if (NULL!=(ptr)) {delete[] ptr;  ptr=NULL;}}
const double PI = boost::math::constants::pi<double>();
#endif //VQANOREF_FEMACROS_HPP
