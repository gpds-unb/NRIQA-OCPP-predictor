Contents
========

1.  [Description](#description)
2.  [License](#license)


Description
===========

This code implements the paper "*No-Reference Image Quality Assessment Using
Orthogonal Color Planes Patterns*". This is not the same implementation used for
generating the results reported in the paper.  

Citation:

    @article{freitas2018no,
        title={No-reference image quality assessment using orthogonal color planes patterns},
        author={Freitas, Pedro Garcia and Akamine, Welington YL and Farias, Mylene CQ},
        journal={IEEE Transactions on Multimedia},
        volume={20},
        number={12},
        pages={3353--3360},
        year={2018},
        publisher={IEEE}
    }



License
=======

This project is licensed under [![badge-license]][link-license]


How to run
==========

```bash
pip install Cython numpy scikit-image scikit-learn
python setup.py build_ext --inplace
python predict.py assessed_image.bmp
```


Thanks
======

Special thanks to [Mylene C. Q. Farias][12].

[12]: http://ene.unb.br/mylene/
[13]: http://www.cisra.com.au/about.html
[link-license]: https://opensource.org/licenses/MIT "MIT license"
[badge-license]: https://img.shields.io/badge/License-MIT-yellow.svg