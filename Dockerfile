FROM continuumio/miniconda3


RUN apt-get update && apt-get install -y \
 libpq-dev \
 build-essential \
 libgc-dev \
&& rm -rf /var/lib/apt/lists/*

RUN conda install cython -y
RUN conda install opencv -y
RUN conda install scikit-image -y
RUN conda install scikit-learn -y

ADD . /code

WORKDIR /code

RUN /bin/bash -c python setup.py build_ext --inplace


CMD ["python", "/code/predict.py"]
