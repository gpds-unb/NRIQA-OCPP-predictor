//
// Created by pedro on 25/09/15.
//

#include "SpatioTemporalOrthonormalPlanesLBP.hpp"
#include "SpatioTemporalOrthonormalPlanesLBPWithInterpolation.hpp"
#include "FEMacros.hpp"

void SpatioTemporalOrthonormalPlanesLBPWithInterpolation::computeLBPTOPWithInterpolation() {
    for (int timeWindow = this->TimeLength; timeWindow < this->Length - this->TimeLength; timeWindow++) {
        for (int yCentral = this->BoderLength; yCentral < this->height - this->BoderLength; yCentral++) {
            for (int xCentral = this->BoderLength; xCentral < this->width - this->BoderLength; xCentral++) {
                uchar centralPointValue = this->data[timeWindow][yCentral][xCentral];
                computeXYPlane(timeWindow, yCentral, xCentral, centralPointValue);
                computeXTPlane(timeWindow, yCentral, xCentral, centralPointValue);
                computeYTPlane(timeWindow, yCentral, xCentral, centralPointValue);
            }
        }
    }
}

void SpatioTemporalOrthonormalPlanesLBPWithInterpolation::computeYTPlane(int t, int y, int x, uchar centralValue) {
    int basicLBP = 0;
    int featureBINs = 0;

    for (int p = 0; p < this->YTNeighborPoints; p++) {
        float y1 = float(y - this->FyRadius * sin(2 * PI * p / this->YTNeighborPoints));
        float z1 = float(t + this->TInterval * cos(2 * PI * p / this->YTNeighborPoints));
        float u = y1 - (int) y1;
        float v = z1 - (int) z1;
        int ltx = (int) floor(y1);
        int lty = (int) floor(z1);
        int lbx = (int) floor(y1);
        int lby = (int) ceil(z1);
        int rtx = (int) ceil(y1);
        int rty = (int) floor(z1);
        int rbx = (int) ceil(y1);
        int rby = (int) ceil(z1);
        float cb1 = this->data[lty][ltx][x] * (1 - u) * (1 - v);
        float cb2 = this->data[lby][lbx][x] * (1 - u) * v;
        float cb3 = this->data[rty][rtx][x] * u * (1 - v);
        float cb4 = this->data[rby][rbx][x] * u * v;
        uchar currentValue = (uchar) (cb1 + cb2 + cb3 + cb4);
        if (currentValue >= centralValue)
            basicLBP += POW (featureBINs);
        featureBINs++;
    }

    if (this->Bincount == 0)
        this->Histogram[2][basicLBP]++;
    else
        this->Histogram[2][this->rotationInvariantLookupTable[basicLBP]]++;
}

void SpatioTemporalOrthonormalPlanesLBPWithInterpolation::computeXTPlane(int t, int y, int x, uchar centralValue) {
    int basicLBP = 0;
    int featureBINs = 0;
    for (int p = 0; p < this->XTNeighborPoints; p++) {
        float x1 = float(x + this->FxRadius * cos(2 * PI * p / this->XTNeighborPoints));
        float z1 = float(t + this->TInterval * sin(2 * PI * p / this->XTNeighborPoints));
        float u = x1 - (int) x1;
        float v = z1 - (int) z1;
        int ltx = (int) floor(x1);
        int lty = (int) floor(z1);
        int lbx = (int) floor(x1);
        int lby = (int) ceil(z1);
        int rtx = (int) ceil(x1);
        int rty = (int) floor(z1);
        int rbx = (int) ceil(x1);
        int rby = (int) ceil(z1);
        float cb1 = this->data[lty][y][ltx] * (1 - u) * (1 - v);
        float cb2 = this->data[lby][y][lbx] * (1 - u) * v;
        float cb3 = this->data[rty][y][rtx] * u * (1 - v);
        float cb4 = this->data[rby][y][rbx] * u * v;
        uchar currrentValue = (uchar) (cb1 + cb2 + cb3 + cb4);
        if (currrentValue >= centralValue)
            basicLBP += POW (featureBINs);
        featureBINs++;
    }

    if (this->Bincount == 0)
        this->Histogram[1][basicLBP]++;
    else
        this->Histogram[1][this->rotationInvariantLookupTable[basicLBP]]++;
}

void SpatioTemporalOrthonormalPlanesLBPWithInterpolation::computeXYPlane(int t, int y, int x, uchar centralValue) {
    int basicLBP = 0;
    int featureBINs = 0;

    for (int p = 0; p < this->XYNeighborPoints; p++) {
        float x1 = float(x + this->FxRadius * cos(2 * PI * p / this->XYNeighborPoints));
        float y1 = float(y - this->FyRadius * sin(2 * PI * p / this->XYNeighborPoints));
        float u = x1 - (int) x1;
        float v = y1 - (int) y1;
        int ltx = (int) floor(x1);
        int lty = (int) floor(y1);
        int lbx = (int) floor(x1);
        int lby = (int) ceil(y1);
        int rtx = (int) ceil(x1);
        int rty = (int) floor(y1);
        int rbx = (int) ceil(x1);
        int rby = (int) ceil(y1);

        float cb1 = this->data[t][lty][ltx] * (1 - u) * (1 - v);
        float cb2 = this->data[t][lby][lbx] * (1 - u) * v;
        float cb3 = this->data[t][rty][rtx] * u * (1 - v);
        float cb4 = this->data[t][rby][rbx] * u * v;
        uchar currentValue = (uchar) (cb1 + cb2 + cb3 + cb4);
        if (currentValue >= centralValue)
            basicLBP += POW (featureBINs);
        featureBINs++;
    }
    if (this->Bincount == 0)
        this->Histogram[0][basicLBP]++;
    else
        this->Histogram[0][this->rotationInvariantLookupTable[basicLBP]]++;
}

void SpatioTemporalOrthonormalPlanesLBPWithInterpolation::computeLBPTOP() {
    computeLBPTOPWithInterpolation();
    normalizeHistogram();
}

SpatioTemporalOrthonormalPlanesLBPWithInterpolation::SpatioTemporalOrthonormalPlanesLBPWithInterpolation(Mat *m,
                                                                                                             int i)
        : SpatioTemporalOrthonormalPlanesLBP(m, i) {
}

SpatioTemporalOrthonormalPlanesLBPWithInterpolation::SpatioTemporalOrthonormalPlanesLBPWithInterpolation(
        vector<Mat> v) : SpatioTemporalOrthonormalPlanesLBP(v) {
}

SpatioTemporalOrthonormalPlanesLBPWithInterpolation::SpatioTemporalOrthonormalPlanesLBPWithInterpolation(
        Mat m) : SpatioTemporalOrthonormalPlanesLBP(m) {
}
