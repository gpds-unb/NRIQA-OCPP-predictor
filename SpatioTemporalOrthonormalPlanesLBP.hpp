//
// Created by pedro on 24/09/15.
//

#ifndef VQANOREF_SPATIOTEMPORALORTONORMALPLANESLBP_HPP
#define VQANOREF_SPATIOTEMPORALORTONORMALPLANESLBP_HPP

#include <opencv2/opencv.hpp>
#include <vector>
#include "AbstractLBPFeatureExtractor.hpp"

using namespace cv;
using namespace std;

/**
 * FeatureExtractors::SpatioTemporalOrthonormalPlanesLBP
 * SpatioTemporalOrthonormalPlanesLBP.hpp
 *
 * Purpose: Compute the LBP-TOP distribution of one video sequence
 *
 * @author Pedro Garcia Freitas
 */
class SpatioTemporalOrthonormalPlanesLBP : public AbstractLBPFeatureExtractor {
    public:
        SpatioTemporalOrthonormalPlanesLBP(Mat *, int);

        SpatioTemporalOrthonormalPlanesLBP(vector<Mat>);

        SpatioTemporalOrthonormalPlanesLBP(Mat);

        Mat getFeatures(void);

        void setFxRadius(int);

        void setFyRadius(int);

        void setTInterval(int);

        void setXYNeighborPoints(int);

        void setXTNeighborPoints(int);

        void setYTNeighborPoints(int);

        void setTimeLength(int);

        void setBoderLength(int);

        void setBincount(int);

    private:
        void computeLBPTOPWithoutInterpolation(void);

        int *extractDefaultBinCount(int, int, int) const;

        int *extractBinCount(int) const;

        void computeLBPinXYPlane(int, int, int, uchar);

        void computeLBPinXTPlane(int, int, int, uchar);

        void computeLBPinYTPlane(int, int, int, uchar);

        void initializeHistogram(int);

        Mat convertHistogramToMat(int);


    protected:
        int FxRadius;
        int FyRadius;
        int TInterval;
        int XYNeighborPoints;
        int XTNeighborPoints;
        int YTNeighborPoints;
        int TimeLength;
        int BoderLength;
        int Bincount;
        float **Histogram;
        uchar ***data;

        void loadDefaultProperties(void);

        void createDataArray(void);

        virtual void computeLBPTOP();

        virtual void init(Mat*, int);

        void normalizeHistogram();

        // Uniform pattern for LBP without rotation invariance for neighboring points 8 (only for 8)
        // If the number of neighboring points is 4, 16 or other numbers than 8, you need to change this lookup
        // array accordingly.
        const int rotationInvariantLookupTable[256] = {
                1, 2, 3, 4, 5, 0, 6, 7, 8, 0, 0, 0, 9, 0, 10, 11,
                12, 0, 0, 0, 0, 0, 0, 0, 13, 0, 0, 0, 14, 0, 15, 16,
                17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                18, 0, 0, 0, 0, 0, 0, 0, 19, 0, 0, 0, 20, 0, 21, 22,
                23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                25, 0, 0, 0, 0, 0, 0, 0, 26, 0, 0, 0, 27, 0, 28, 29,
                30, 31, 0, 32, 0, 0, 0, 33, 0, 0, 0, 0, 0, 0, 0, 34,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 35,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 36,
                37, 38, 0, 39, 0, 0, 0, 40, 0, 0, 0, 0, 0, 0, 0, 41,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 42,
                43, 44, 0, 45, 0, 0, 0, 46, 0, 0, 0, 0, 0, 0, 0, 47,
                48, 49, 0, 50, 0, 0, 0, 51, 52, 53, 0, 54, 55, 56, 57, 58
        };
};
#endif //VQANOREF_SPATIOTEMPORALORTONORMALPLANESLBP_HPP
