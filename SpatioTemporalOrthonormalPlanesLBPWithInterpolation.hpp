//
// Created by pedro on 25/09/15.
//

#ifndef VQANOREF_SPATIOTEMPORALORTHONORMALPLANESLBPWITHBILINEARINTERPOLATION_HPP
#define VQANOREF_SPATIOTEMPORALORTHONORMALPLANESLBPWITHBILINEARINTERPOLATION_HPP

#include <opencv2/opencv.hpp>
#include <vector>
#include "SpatioTemporalOrthonormalPlanesLBP.hpp"

using namespace std;
using namespace cv;

class SpatioTemporalOrthonormalPlanesLBPWithInterpolation : public SpatioTemporalOrthonormalPlanesLBP {
    public:
        SpatioTemporalOrthonormalPlanesLBPWithInterpolation(Mat *, int);
        SpatioTemporalOrthonormalPlanesLBPWithInterpolation(vector<Mat>);
        SpatioTemporalOrthonormalPlanesLBPWithInterpolation(Mat);

    protected:
        virtual void computeLBPTOP();

    private:
        void computeLBPTOPWithInterpolation();
        void computeXYPlane(int, int, int, uchar);
        void computeXTPlane(int, int, int, uchar);
        void computeYTPlane(int, int, int, uchar);
};

#endif //VQANOREF_SPATIOTEMPORALORTHONORMALPLANESLBPWITHBILINEARINTERPOLATION_HPP
