#ifndef VQANOREF_ABSTRACTLBPFEATUREEXTRACTOR_HPP
#define VQANOREF_ABSTRACTLBPFEATUREEXTRACTOR_HPP

#include <opencv2/opencv.hpp>
#include <vector>

using namespace cv;
using namespace std;

class AbstractLBPFeatureExtractor {
    public:
        AbstractLBPFeatureExtractor(Mat*, int);
        AbstractLBPFeatureExtractor(vector<Mat>);
        AbstractLBPFeatureExtractor(Mat);

        virtual Mat getFeatures(void) = 0;

        void setFrames(Mat *, int);

        void setLength(int);

        void setHeight(int);

        void setWidth(int);

    protected:
        int Length;
        Mat *greyImg;
        int height;
        int width;

        virtual void init(Mat *, int);

        virtual void loadDefaultProperties(void) = 0;
};
#endif //VQANOREF_ABSTRACTLBPFEATUREEXTRACTOR_HPP
