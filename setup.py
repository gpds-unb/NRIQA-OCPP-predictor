from distutils.command import clean
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
import numpy
import sys


libdr = ['/usr/lib', '/usr/local/lib', '/usr/lib64',]
incdr = [numpy.get_include(), "/usr/include/", "/usr/local/include/"]
compiler_args = ['-O2', '-mtune=native', '-march=native', '-std=c++11', '-fopenmp']
opencvlibs = ['opencv_core', 'opencv_imgproc', 'opencv_imgcodecs']

opt_force = True

ext_modules = [
    Extension("cvt", ["cvt.pyx"],
              language = "c++",
              extra_compile_args=["-std=c++11"],
              include_dirs=incdr,
              library_dirs = libdr,
              libraries=["opencv_core"]
    ),
    Extension(
        "ocpp",
        sources=[
            "ocpp.pyx",
            "AbstractLBPFeatureExtractor.cpp",
            "SpatioTemporalOrthonormalPlanesLBP.cpp",
            "SpatioTemporalOrthonormalPlanesLBPWithInterpolation.cpp"
        ],
        language="c++",
        include_dirs=incdr,
        library_dirs=libdr,
        extra_compile_args=compiler_args,
        libraries=opencvlibs + ['gomp', 'gc']
    )
]

forcing = "--force" in sys.argv

setup(
    name='LVP',
    version='0.1',
    url='http://www.sawp.com.br',
    license='GPLv2',
    author='pedro',
    author_email='sawp@sawp.com.br',
    description='No-reference Image Quality Assessment using ONPLBP.',
    cmdclass={'build_ext': build_ext},
    ext_modules=cythonize(ext_modules, force=forcing),
    requires=['numpy', 'scipy']
)
