//
// Created by pedro on 24/09/15.
//

#include "SpatioTemporalOrthonormalPlanesLBP.hpp"
#include "StdAfx.h"
#include "FEMacros.hpp"
#include <gc_cpp.h>


Mat SpatioTemporalOrthonormalPlanesLBP::getFeatures(void) {
    createDataArray();
    int DimLBPTOP = (Bincount == 0) ? POW(XYNeighborPoints) : Bincount;
    initializeHistogram(DimLBPTOP);
    computeLBPTOP();
    return convertHistogramToMat(DimLBPTOP);
}

Mat SpatioTemporalOrthonormalPlanesLBP::convertHistogramToMat(int DimLBPTOP) {
    Mat A;
    A = Mat::zeros(3, DimLBPTOP, CV_32FC1);
    for (int j = 0; j < 3; j++) {
        for (int k = 0; k < DimLBPTOP; k++) {
            A.ptr<float>(j)[k] = this->Histogram[j][k];
        }
    }
    return A;
}

void SpatioTemporalOrthonormalPlanesLBP::initializeHistogram(int DimLBPTOP) {
    this->Histogram = New2DPointer<float>(3, DimLBPTOP); // 3 planes with uniform patterns
    for (int j = 0; j < 3; j++) {
        for (int k = 0; k < DimLBPTOP; k++) {
            this->Histogram[j][k] = 0;
        }
    }
}

void SpatioTemporalOrthonormalPlanesLBP::computeLBPTOP() {
    computeLBPTOPWithoutInterpolation();
    normalizeHistogram();
}

void SpatioTemporalOrthonormalPlanesLBP::normalizeHistogram() {
    int xy = XYNeighborPoints;
    int xt = XTNeighborPoints;
    int yt = YTNeighborPoints;
    int *binCount = (Bincount == 0) ? extractDefaultBinCount(xy, xt, yt) : extractBinCount(Bincount);

    // Normaliztion
    for (int j = 0; j < 3; j++) {
        int Total = 0;
        for (int i = 0; i < binCount[j]; i++)
            Total += (int) Histogram[j][i];
        for (int i = 0; i < binCount[j]; i++) {
            Histogram[j][i] /= Total;
        }
    }
}

int *SpatioTemporalOrthonormalPlanesLBP::extractDefaultBinCount(int xyNeighborPoints, int xtNeighborPoints,
                                                                    int ytNeighborPoints) const {
    int *binCount = new(UseGC) int[3];
    binCount[0] = POW(xyNeighborPoints);
    binCount[1] = POW(xtNeighborPoints);
    binCount[2] = POW(ytNeighborPoints);
    return binCount;
}

int *SpatioTemporalOrthonormalPlanesLBP::extractBinCount(int b) const {
    int *binCount = new(UseGC) int[3];
    binCount[0] = b;
    binCount[1] = b;
    binCount[2] = b;
    return binCount;
}

void SpatioTemporalOrthonormalPlanesLBP::computeLBPTOPWithoutInterpolation() {
    for (int tCentral = TimeLength; tCentral < Length - TimeLength; tCentral++) {
        for (int yCentral = BoderLength; yCentral < height - BoderLength; yCentral++) {
            for (int xCentral = BoderLength; xCentral < width - BoderLength; xCentral++) {
                uchar centralPointValue = data[tCentral][yCentral][xCentral];
                computeLBPinXYPlane(tCentral, yCentral, xCentral, centralPointValue);
                computeLBPinXTPlane(tCentral, yCentral, xCentral, centralPointValue);
                computeLBPinYTPlane(tCentral, yCentral, xCentral, centralPointValue);
            }
        }
    }
}

void SpatioTemporalOrthonormalPlanesLBP::computeLBPinYTPlane(int t, int y, int x, uchar centralValue) {
    int BasicLBP = 0;
    int FeaBin = 0;
    for (int p = 0; p < this->YTNeighborPoints; p++) {
        int Y = int(y - this->FyRadius * sin(2 * PI * p / this->YTNeighborPoints) + 0.5);
        int Z = int(t + this->TInterval * cos(2 * PI * p / this->YTNeighborPoints) + 0.5);
        BOUND(Y, 0, this->height - 1);
        BOUND(Z, 0, this->Length - 1);
        uchar currentValue = this->data[Z][Y][x];
        if (currentValue >= centralValue)
            BasicLBP += POW (FeaBin);
        FeaBin++;
    }

    if (this->Bincount == 0)
        this->Histogram[2][BasicLBP]++;
    else
        this->Histogram[2][this->rotationInvariantLookupTable[BasicLBP]]++;
}

void SpatioTemporalOrthonormalPlanesLBP::computeLBPinXTPlane(int i, int yc, int xc, uchar centralValue) {
    int basicLBP = 0;
    int featureBINs = 0;
    for (int p = 0; p < this->XTNeighborPoints; p++) {
        int X = int(xc + this->FxRadius * cos(2 * PI * p / this->XTNeighborPoints) + 0.5);
        int Z = int(i + this->TInterval * sin(2 * PI * p / this->XTNeighborPoints) + 0.5);
        BOUND(X, 0, this->width - 1);
        BOUND(Z, 0, this->Length - 1);
        uchar currentValue = this->data[Z][yc][X];
        if (currentValue >= centralValue)
            basicLBP += POW (featureBINs);
        featureBINs++;
    }

    if (this->Bincount == 0)
        this->Histogram[1][basicLBP]++;
    else
        this->Histogram[1][this->rotationInvariantLookupTable[basicLBP]]++;
}

void SpatioTemporalOrthonormalPlanesLBP::computeLBPinXYPlane(int i, int yc, int xc, uchar centralValue) {
    int BasicLBP = 0;
    int FeaBin = 0;

    for (int p = 0; p < this->XYNeighborPoints; p++) {
        int X = int(xc + this->FxRadius * cos(2 * PI * p / this->XYNeighborPoints) + 0.5);
        int Y = int(yc - this->FyRadius * sin(2 * PI * p / this->XYNeighborPoints) + 0.5);
        BOUND(X, 0, this->width - 1);
        BOUND(Y, 0, this->height - 1);
        uchar currentValue = this->data[i][Y][X];
        if (currentValue >= centralValue) BasicLBP += POW (FeaBin);
        FeaBin++;
    }

    if (this->Bincount == 0)
        this->Histogram[0][BasicLBP]++;
    else
        this->Histogram[0][this->rotationInvariantLookupTable[BasicLBP]]++;
}

void SpatioTemporalOrthonormalPlanesLBP::createDataArray(void) {
    int widthstep = greyImg[0].step[0];
    uchar **fg = new(UseGC) uchar *[Length];
    data = new(UseGC) uchar **[Length];

    for (int i = 0; i < Length; i++) {
        fg[i] = (uchar *) greyImg[i].data;
    }

    for (int i = 0; i < Length; i++) {
        data[i] = New2DPointer<uchar>(height, width);
    }

    for (int i = 0; i < Length; i++) {
        for (int j = 0; j < height; j++) {
            for (int k = 0; k < width; k++) {
                data[i][j][k] = *(fg[i] + widthstep * j + k);
            }
        }
    }
}

SpatioTemporalOrthonormalPlanesLBP::SpatioTemporalOrthonormalPlanesLBP(Mat *mat, int i)
        : AbstractLBPFeatureExtractor(mat, i) {
    init(mat, i);
}

SpatioTemporalOrthonormalPlanesLBP::SpatioTemporalOrthonormalPlanesLBP(vector<Mat> v) : AbstractLBPFeatureExtractor(
        v) {
    init(&v[0], v.size());
}

SpatioTemporalOrthonormalPlanesLBP::SpatioTemporalOrthonormalPlanesLBP(Mat m) : AbstractLBPFeatureExtractor(
        m) {
    int channels = m.channels();
    vector<Mat> frames(channels);
    split(m, frames);
    init(&frames[0], frames.size());
}


void SpatioTemporalOrthonormalPlanesLBP::setFxRadius(int FxRadius) {
    SpatioTemporalOrthonormalPlanesLBP::FxRadius = FxRadius;
}

void SpatioTemporalOrthonormalPlanesLBP::setFyRadius(int FyRadius) {
    SpatioTemporalOrthonormalPlanesLBP::FyRadius = FyRadius;
}

void SpatioTemporalOrthonormalPlanesLBP::setTInterval(int TInterval) {
    SpatioTemporalOrthonormalPlanesLBP::TInterval = TInterval;
}

void SpatioTemporalOrthonormalPlanesLBP::setXYNeighborPoints(int XYNeighborPoints) {
    SpatioTemporalOrthonormalPlanesLBP::XYNeighborPoints = XYNeighborPoints;
}

void SpatioTemporalOrthonormalPlanesLBP::setXTNeighborPoints(int XTNeighborPoints) {
    SpatioTemporalOrthonormalPlanesLBP::XTNeighborPoints = XTNeighborPoints;
}

void SpatioTemporalOrthonormalPlanesLBP::setYTNeighborPoints(int YTNeighborPoints) {
    SpatioTemporalOrthonormalPlanesLBP::YTNeighborPoints = YTNeighborPoints;
}

void SpatioTemporalOrthonormalPlanesLBP::setTimeLength(int TimeLength) {
    SpatioTemporalOrthonormalPlanesLBP::TimeLength = TimeLength;
}

void SpatioTemporalOrthonormalPlanesLBP::setBoderLength(int BoderLength) {
    SpatioTemporalOrthonormalPlanesLBP::BoderLength = BoderLength;
}

void SpatioTemporalOrthonormalPlanesLBP::setBincount(int Bincount) {
    SpatioTemporalOrthonormalPlanesLBP::Bincount = Bincount;
}


void SpatioTemporalOrthonormalPlanesLBP::loadDefaultProperties(void) {
    setFxRadius(1);
    setFyRadius(1);
    setTInterval(1);
    setTimeLength(1);
    setBoderLength(2);
    setXYNeighborPoints(8);
    setXTNeighborPoints(8);
    setYTNeighborPoints(8);
    setBincount(0);
}

void SpatioTemporalOrthonormalPlanesLBP::init(Mat *mat, int i) {
    loadDefaultProperties();
}
