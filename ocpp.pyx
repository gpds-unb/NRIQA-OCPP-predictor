from cvt cimport *


cdef extern from "AbstractLBPFeatureExtractor.hpp":
    cdef cppclass AbstractLBPFeatureExtractor:
        pass


cdef extern from "SpatioTemporalOrthonormalPlanesLBP.hpp":
    cdef cppclass SpatioTemporalOrthonormalPlanesLBP:
        SpatioTemporalOrthonormalPlanesLBP(Mat)
        Mat getFeatures()


cdef extern from "SpatioTemporalOrthonormalPlanesLBPWithInterpolation.hpp":
    cdef cppclass SpatioTemporalOrthonormalPlanesLBPWithInterpolation:
        SpatioTemporalOrthonormalPlanesLBPWithInterpolation(Mat)
        Mat getFeatures()


# WRAPPER
cdef class OCPPWRAPPER:
    cdef SpatioTemporalOrthonormalPlanesLBP *classptr

    def __cinit__(self, channels):
        cdef Mat tmp = nparray2cvmat(channels)
        self.classptr = new SpatioTemporalOrthonormalPlanesLBP(tmp)

    def __dealloc__(self):
        del self.classptr

    def getFeatures(self):
        cdef Mat ltp_feature = self.classptr.getFeatures()
        return cvmat2nparray(ltp_feature)


# WRAPPER
cdef class ONPLBPWithInterpolationWRAPPER:
    cdef SpatioTemporalOrthonormalPlanesLBPWithInterpolation *classptr

    def __cinit__(self, channels):
        cdef Mat tmp = nparray2cvmat(channels)
        self.classptr = \
            new SpatioTemporalOrthonormalPlanesLBPWithInterpolation(tmp)

    def __dealloc__(self):
        del self.classptr

    def getFeatures(self):
        cdef Mat ltp_feature = self.classptr.getFeatures()
        return cvmat2nparray(ltp_feature)


def OCPP(np.ndarray img, size=8):
    wrapper = OCPPWRAPPER(img)
    features = wrapper.getFeatures()
    return features


def OCPPWithInterpolation(np.ndarray img, size=8):
    wrapper = ONPLBPWithInterpolationWRAPPER(img)
    return wrapper.getFeatures()
